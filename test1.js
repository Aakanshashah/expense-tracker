let selectedRowIncome = null;
let selectedRowExpense = null;
let incomeInput, totalInput;
let valueHolder, expenseInput;
incomeInput = parseFloat(document.getElementById('incomeInput').value);
totalInput = parseFloat(document.getElementById('totalBalance').value) || 0;
expenseInput = parseFloat(document.getElementById('expenseInput').value);

function addIncome() {

    incomeInput = parseFloat(document.getElementById('incomeInput').value);
    totalInput = parseFloat(document.getElementById('totalBalance').value) || 0;
    Discription = document.getElementById("incomeDiscription").value;
    if (!incomeInput || !Discription) {
        alert('fill all inputs');
    } else {

        valueHolder = document.getElementById('totalBalance').value = incomeInput + totalInput;
        let formDataIncome = readInputData();
        if (selectedRowIncome == null)
            insertNewInputRecord(formDataIncome);
        else
            updateInputRecord(formDataIncome);
    }
    resetPage();

}

function subtractExpense() {
    expenseInput = parseFloat(document.getElementById('expenseInput').value);
    totalInput = parseFloat(document.getElementById('totalBalance').value) || 0;
    Discription = document.getElementById("expenseDiscription").value;
    if (!expenseInput || !Discription) {
        alert('fill all inputs');
    } else {

        valueHolder = document.getElementById('totalBalance').value = totalInput - expenseInput;
        let formDataExpense = readExpenseData();
        if (selectedRowExpense == null)
            insertNewExpenseRecord(formDataExpense);
        else
            updateExpenseRecord(formDataExpense);
    }
    resetPage();
}

function readInputData() {
    let formDataIncome = {};
    formDataIncome["incomeInput"] = document.getElementById("incomeInput").value;
    formDataIncome["incomeDiscription"] = document.getElementById("incomeDiscription").value;
    return formDataIncome;
}

function readExpenseData() {
    let formDataExpense = {};
    formDataExpense["expenseInput"] = document.getElementById("expenseInput").value;
    formDataExpense["expenseDiscription"] = document.getElementById("expenseDiscription").value;
    return formDataExpense;
}

function insertNewInputRecord(data) {
    let table = document.getElementById('incomeList').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.incomeInput;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.incomeDiscription;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = ` <button onclick="editInput(this);">Edit</button> <button onclick="deleteIncome(this)">Delete</button>`;

}

function insertNewExpenseRecord(data) {
    let table = document.getElementById('ExpenseList').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.expenseInput;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.expenseDiscription;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = ` <button onclick="editExpense(this);">Edit</button> <button onclick="deleteExpense(this)">Delete</button>`;

}

function resetPage() {
    document.getElementById("incomeInput").value = "";
    document.getElementById("incomeDiscription").value = "";
    document.getElementById("expenseInput").value = "";
    document.getElementById("expenseDiscription").value = "";
    selectedRowIncome = null;
    selectedRowExpense = null;
}

function editInput(td) {
    selectedRowIncome = td.parentElement.parentElement;
    a = td.parentElement.parentElement.children;
    value = parseFloat(a[0].outerText);
    document.getElementById('incomeInput').value = selectedRowIncome.cells[0].innerHTML;
    document.getElementById('incomeDiscription').value = selectedRowIncome.cells[1].innerHTML;
    valueHolder = document.getElementById('totalBalance').value = valueHolder - value;
}

function editExpense(td) {
    selectedRowExpense = td.parentElement.parentElement;
    a = td.parentElement.parentElement.children;
    value = parseFloat(a[0].innerText);
    document.getElementById('expenseInput').value = selectedRowExpense.cells[0].innerHTML;
    document.getElementById('expenseDiscription').value = selectedRowExpense.cells[1].innerHTML;
    valueHolder = document.getElementById('totalBalance').value = valueHolder + value;
}

function updateInputRecord(formDataIncome) {
    selectedRowIncome.cells[0].innerHTML = formDataIncome.incomeInput;
    selectedRowIncome.cells[1].innerHTML = formDataIncome.incomeDiscription;
}

function updateExpenseRecord(formDataExpense) {
    selectedRowExpense.cells[0].innerHTML = formDataExpense.expenseInput;
    selectedRowExpense.cells[1].innerHTML = formDataExpense.expenseDiscription;
}

function deleteIncome(td) {
    if (confirm('are you sure to delete this record?')) {
        row = td.parentElement.parentElement;
        a = td.parentElement.parentElement.children;
        value = parseFloat(a[0].outerText);
        a = td.parentElement.parentElement.children;
        valueHolder = document.getElementById('totalBalance').value = valueHolder - value;
        document.getElementById('incomeList').deleteRow(row.rowIndex);
        resetPage();
    }
}

function deleteExpense(td) {
    if (confirm('are you sure to delete this record?')) {
        row = td.parentElement.parentElement;
        a = td.parentElement.parentElement.children;
        value = parseFloat(a[0].outerText);
        valueHolder = document.getElementById('totalBalance').value = valueHolder + value;
        document.getElementById('ExpenseList').deleteRow(row.rowIndex);
        resetPage();
    }
}